build:
	ls -d */ | xargs -I@ bash -c "cd @ && make verify.makefile.properties && make build"
publish:
	ls -d */ | xargs -I@ bash -c "cd @ && make verify.makefile.properties && make publish"
verify.makefile.properties:
	@if ! [ -f $(CURDIR)/Makefile.properties ]; then \
		cp $(CURDIR)/sample.properties $(CURDIR)/Makefile.properties; \
		$(MAKE) log.error MSG="Makefile.properties wasn't found so it was created. Re-run the same command to complete the action."; \
		exit 1; \
	fi