# CI Images
This repository is a collection of images for use in your GitLab CI pipeline.

## Image Listing
- [Docker Image](#docker-image)
- [Heroku Image](#heroku-image)

## Docker Image
> DockerHub link: https://hub.docker.com/r/usvc/docker

The Docker image is used for running Docker-in-Docker within your pipeline. Use at your own risk!

### Usage

```yaml
some job:
  stage: some stage
  image: usvc/docker:latest
  services:
  - docker:dind
  script:
  - docker version
```

## Heroku Image
> DockerHub link: https://hub.docker.com/r/usvc/heroku

The Heroku image is used for running an authenticated Heroku CLI within the pipeline.

### Pre-Usage
Since the Heroku CLI blocks us from using string authentications, we need to generate the authentciation tokens first and insert them as environment variables into the image. To do this, clone this repository and navigate into the `./heroku` directory.

Check out the `Makefile` and make sure you're comfortable with what you're running (assuming I have handed this off to someone else, they could inject code to send your credentials back to themselves).

Run `make login` and log into your Heroku account. In the `./secrets` directory, you should observe a `b64.netrc` appear. Copy the contents and insert it into your project's CI/CD Variables with the environment variable name `NETRC_B64` so that it is injected into the pipeline when using the image. The image contains a pre-heroku hook to inject your authentication before running the Heroku CLI. 

### Usage

```yaml
some job:
  stage: some stage
  image: usvc/heroku:latest
  script:
  - heroku version
```